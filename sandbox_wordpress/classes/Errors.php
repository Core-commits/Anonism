<?php
/**
* Class and Function List:
* Function list:
* - _404()
* - _403()
* - _402()
* - _401()
* - _400()
* Classes list:
* - Errors
*/
class Errors
  {
    static function _bringBack(){
        echo "
        <script>
        const delay = ms => new Promise(res => setTimeout(res, ms));
        async function goBack(){
            await delay(5000);
            window.location = '/';
        }
        goBack();
        </script>
        ";
    }
    static function _404()
      {
        echo "Uh oh! We can't find the page! Going back!";
        self::_bringBack();

      }
    static function _403()
      {
        echo "Uh oh! You can't see this page! Forbbiden!";
        self::_bringBack();

      }
    static function _402()
      {
        echo "You need to pay to see this content";
            self::_bringBack();


      }
    static function _401()
      {
        echo "There was a failure authenticating your user! Try logging in again!";
        self::_bringBack();

      }
    static function _400()
      {
        echo "There was an error processing your request, make sure it's properly done.";
        self::_bringBack();

      }
  }
?>
