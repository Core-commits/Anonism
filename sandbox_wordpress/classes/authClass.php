<?php
include __DIR__.'/Errors.php';
include __DIR__.'/sessionClass.php';
class Auth
{
    static function logOut()
    {        
        session_start(); // Restart the session...
        $_SESSION = []; // Empty it...
        session_unset();
        session_destroy(); // Destroy the session...
        session_start(); // Restart it...
        // Boom logged out.
    }
    static function login($username, $password)
    {
        // First lets make sure he's not logged in.

        $loggedIn = self::checkAuth();
        if (!$loggedIn)
        {

        }
    }
    static function register($username, $password, $email)
    {
        
    }
    static function checkAuth()
    {
        session_start();
        $session = isset($_SESSION["user"]);
        return $session;
        
    }
    static function checkAdmin()
    {

        /**
         * We check if the user is logged in first. Then we check for the admin.
         */
        $isLogged = self::checkAuth(); // Is he logged in?
        $data = Sess::getInstance(); 
        $exists = $data->__isset("isAdmin");
        if(!$isLogged) { echo "You need to be logged in!"; die;}
        if (!$exists) return self::logOut(); // We need that data.
        if ($isLogged && $data->isAdmin) return true; else return false;
    }
    static function checkValidCreds()
    {

    }
    static function isBanned($username)
    {
        $isLogged = self::checkAuth();
        $isAdmin = self::checkAdmin();

        if ($isLogged && $isAdmin) return false; // He's admin, admins cant be banned.
        if ($isLogged && !$isAdmin)
        {
            // Look for them on DB
            
        }
    }
}

?>
