<?php
/**
* Class and Function List:
* Function list:
* - onlyLogged()
* - onlyAdmin()
* Classes list:
* - Middleware
*/
class Middleware
  {
    static function onlyLogged()
      {
        $isLogged = Auth::checkAuth();
        if (!$isLogged)
          {
            Errors::_403();
          }
      }
    static function onlyAdmin()
      {
        // ALWAYS DOUBLE LAYER!
        $isLogged = Auth::checkAuth();
        if (!$isLogged)
          {
            Errors::_401();
          }
        $Admin = Auth::checkAdmin();
        if (!$Admin)
          {
            Errors::_403();
          }
      }
  }
?>
