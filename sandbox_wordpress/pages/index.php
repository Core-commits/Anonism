<?php
include 'components/header.php';
include '../classes/authClass.php';
session_start();
$data = Auth::checkAuth();
if($data){
    $username = htmlspecialchars($_SESSION['user']);
    include 'html/loggedIn.php';

} else { 
 include 'html/notLoggedIn.html';
}

include 'components/footer.php';
?>



