<?php
include($_SERVER['DOCUMENT_ROOT'].'/../classes/adminClass.php');

/**
* Class and Function List:
* Function list:
* Classes list:
*
require_once('../../classes/adminClass.php');
*/
$name = AdminStuff::getName();

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <link rel="stylesheet" href="./../static/main.css">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Sandb</title>
    <script src="https://unpkg.com/axios/dist/axios.min.js"></script>
    <script>async function logOut(){ await axios.post('/logOut.php');window.location = '/logIn.php';};function logIn(){window.location = '/logIn.php';};</script>
<nav class="navbar navbar-dark bg-dark">
  <div class="container-fluid">
    <span class="navbar-brand mb-0 h1"><?=$name?></span>
  </div>
</nav>
</head>
<body>
